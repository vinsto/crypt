<?php

namespace Vinsto\Crypt\RSA;

use phpseclib\Crypt\RSA;

class RSAHandler
{
    /**
     * @var RSA $rsa
     */
    private $rsa;
    private $privateKey;
    private $publicKey;
    private $encryptionMode;
    private $signatureMode;

    public function __construct()
    {
        $this->rsa = new RSA();
    }


    public function encrypt(
        string $msg,
        string $publickey,
        string $encryptionMode=RSA::ENCRYPTION_PKCS1)
    {
        $this->rsa->setEncryptionMode($encryptionMode);
        /**
         *
         * 公钥加密
         * 私钥解密
         *
         */
        $this->rsa->loadKey($publickey);
        $en = $this->rsa->encrypt($msg);

        return base64_encode($en);
    }

    public function decrypt(
        string $msg,
        string $privatekey,
        string $encryptionMode=RSA::ENCRYPTION_PKCS1)
    {
        $this->rsa->setEncryptionMode($encryptionMode);
        /**
         *
         * 公钥加密
         * 私钥解密
         *
         */
        $this->rsa->loadKey($privatekey);
        $de = $this->rsa->decrypt(base64_decode($msg));

        return $de;
    }

    public function sign(
        string $msg,
        string $privatekey,
        string $signatureMode=RSA::SIGNATURE_PKCS1)
    {
        $this->rsa->setSignatureMode($signatureMode);
        /**
         *
         * 私钥签名
         * 公钥验证
         *
         */
        $this->rsa->loadKey($privatekey);
        $signature = $this->rsa->sign($msg);

        return base64_encode($signature);
    }

    public function verify(
        string $msg,
        string $signature,
        string $publickey,
        string $signatureMode=RSA::SIGNATURE_PKCS1)
    {
        $this->rsa->setSignatureMode($signatureMode);
        /**
         *
         * 私钥签名
         * 公钥验证
         *
         */
        $this->rsa->loadKey($publickey);
        $signature = base64_decode($signature);

        return $this->rsa->verify($msg, $signature);
    }

///////////////////////////////////////////////////////////
///
/// Getters & Setters
///
/// //////////////////////////////////////////////////////
    /**
     * @return RSA
     */
    public function getRsa(): RSA
    {
        return $this->rsa;
    }

    /**
     * @param RSA $rsa
     */
    public function setRsa(RSA $rsa): void
    {
        $this->rsa = $rsa;
    }

    /**
     * @return mixed
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * @param mixed $privateKey
     */
    public function setPrivateKey($privateKey): void
    {
        $this->privateKey = $privateKey;
    }

    /**
     * @return mixed
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * @param mixed $publicKey
     */
    public function setPublicKey($publicKey): void
    {
        $this->publicKey = $publicKey;
    }

    /**
     * @return mixed
     */
    public function getEncryptionMode()
    {
        return $this->encryptionMode;
    }

    /**
     * @param mixed $encryptionMode
     */
    public function setEncryptionMode($encryptionMode): void
    {
        $this->encryptionMode = $encryptionMode;
    }

    /**
     * @return mixed
     */
    public function getSignatureMode()
    {
        return $this->signatureMode;
    }

    /**
     * @param mixed $signatureMode
     */
    public function setSignatureMode($signatureMode): void
    {
        $this->signatureMode = $signatureMode;
    }


}
